#[macro_use]
extern crate log;

use std::net::{TcpListener, TcpStream};

fn main() {
    let listener = TcpListener::bind("127.0.0.1:8080").unwrap();

    listener.set_nonblocking(true).expect("Cannot set non-blocking");

    fn handle_client(stream: TcpStream) {
        info!(target: "access", "{} {}", stream.peer_addr().unwrap().ip(),
                 stream.peer_addr().unwrap().port());
    }

    // accept connections and process them, spawning a new thread for each one
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                handle_client(stream);
            }
            Err(e) => { /* connection failed */ }
        }
    }
}
